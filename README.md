Environment starter

## FROM DEV => Test Branch

### RUNNER

### VARIABLES

### PIPELINE

- Stages
  - Stages allow you to group jobs and run them in an ordered sequence in the pipeline. They represent the different phases in the software development lifecycle like build, test, deploy etc.
- Jobs
  - Jobs define the work that needs to be done in a CI/CD pipeline. They are made up of script that gets executed.

---

---

```bash

- ssh $SSH_USER@$SERVER_IP sudo docker stop $CONTAINER_NAME || true
- docker images | grep -E '$CONTAINER_NAME' | awk '{print $3}
- |
  export IMAGE_IDS=$(docker images | grep -E $CONTAINER_NAME | awk '{print $3}')
  if [ -n "$IMAGE_IDS" ]; then
    docker rmi -f $IMAGE_IDS
  else
    echo "No $CONTAINER_NAME images found to remove."
  fi
- ssh $SSH_USER@$SERVER_IP sudo rm /etc/caddy/non_production/$CI_PROJECT_NAME/$CONTAINER_NAME.caddy
- ssh $SSH_USER@$SERVER_IP sudo rmdir -p /home/deploy/non_production/$CI_PROJECT_NAME/$SLUG
- ssh $SSH_USER@$SERVER_IP sudo caddy reload --config /etc/caddy/Caddyfile

```

```bash

#!/bin/bash

# List all image IDs that are untagged or have the "latest" tag
image_ids=$(docker images | grep -E 'test_image' | awk '{print $3}')

echo $image_ids
echo "=========="
# Check if there are any image IDs to remove
if [ -n "$image_ids" ]; then
  docker rmi -f $image_ids
else
  echo "No untagged or 'latest' images found to remove."
fi

```
